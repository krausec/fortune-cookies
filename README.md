# EVE Fortune Cookies

```
 __________________________________________________________________________
/ in R, readRDS is up to 78 times faster than read.csv                     \
\   --> https://wiki.ufz.de/eve/index.php?title=The_Ultimate_R_Table_Guide /
 --------------------------------------------------------------------------
        \   ^__^
         \  (OO)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```
