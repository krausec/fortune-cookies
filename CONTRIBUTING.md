New cookie contributions are welcome!

1.  One cookie per commit / merge request.
1.  Please make sure to add the `%` separators.
1.  Please format the cookies that each line has less than 75 characters.
